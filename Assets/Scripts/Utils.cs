﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Utils
{
	public static float Lerp(float from, float to, float t)
	{
		return (1.0f - t) * from + t * to;
	}

	public static float LerpClamped(float from, float to, float t)
	{
		return Lerp(from, to, Mathf.Clamp01(t));
	}

	public static float InverseLerp(float from, float to, float t)
	{
		Debug.Assert(from != to);
		return (t - from) / (to - from);
	}

	public static float InverseLerpClamped(float from, float to, float t)
	{
		return InverseLerp(from, to, Mathf.Clamp01(t));
	}

	public static float Remap(float from, float to, float newFrom, float newTo, float t)
	{
		return Lerp(newFrom, newTo, InverseLerp( from, to, t ));
	}

	public static float RemapClamped(float from, float to, float newFrom, float newTo, float t)
	{
		return LerpClamped(newFrom, newTo, InverseLerp(from, to, t));
	}
	
}
