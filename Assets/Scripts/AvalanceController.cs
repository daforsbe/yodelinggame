﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AvalanceController : MonoBehaviour, InteractiveObject
{
	public GameObject deathParticle;
	public GameObject prefab;

	public bool activated = false;
	public float velocity;
	private bool activateOnCollision = false;
	private bool onGround = false;
	private bool isChild = false;
	private bool activateMeCalled = false;

	private bool timerActivated = false;
	private float timeBeforeObjectCanBeActivated = 0;
	public float maxTimeBeforeObjectCanBeActivated = 1f;

	// Checkpoint variables
	private GameManagerScript GameManager;
	private bool savedStateOfActivated;
	private Vector3 savedPosition;
	private Vector3 savedScale;
	private bool savedStateOfActivateOnCollision;
	private bool savedStateOfOnGround;
	private List<GameObject> savedChilds;
	private int ID;

	private bool savedStateOfTimerActivated;
	private float savedTimeBeforeObjectCanBeActivated;

	void Start()
	{
		if (!isChild)
		{
			GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerScript>();
			GameManager.AddObjectToList(this.gameObject);
			Save();
		}
	}

	void Update()
	{
		if (activateMeCalled)
		{
			activateMeCalled = false;

			if (!isChild)
			{
				if (activateOnCollision)
				{
					activated = true;
					Fabric.EventManager.Instance.PostEvent("AvalanceDrop", Fabric.EventAction.PlaySound, null, this.gameObject);
				}
				else if (!timerActivated)
				{
					timerActivated = true;
					Fabric.EventManager.Instance.PostEvent("AvalanceHit", Fabric.EventAction.PlaySound, null, this.gameObject);
					timeBeforeObjectCanBeActivated = maxTimeBeforeObjectCanBeActivated;
					StartCoroutine(RotateBackAndForth());
				}
			}
		}

		if (timerActivated)
		{
			if(timeBeforeObjectCanBeActivated > 0)
			{
				timeBeforeObjectCanBeActivated -= Time.deltaTime;
				if (timeBeforeObjectCanBeActivated <= 0)
				{
					activateOnCollision = true;
				}
			}
		}
	}

	void FixedUpdate()
	{
		if (activated && !onGround)
		{
			transform.position += Vector3.down * velocity * Time.fixedDeltaTime;
		}
	}

	public void ActivateMe()
	{
		activateMeCalled = true;
	}

	private IEnumerator RotateBackAndForth()
	{
		float targetValue = 1.05f;
		float speed = 35;
		for (int i = 0; i <= 1; i++)
		{
			while (transform.rotation.z + 1 <= targetValue)
			{
				transform.Rotate(new Vector3(0, 0, Time.deltaTime * speed));
				yield return new WaitForSeconds(0.0f);
			}
			while (2 - (transform.rotation.z + 1) <= targetValue)
			{
				transform.Rotate(new Vector3(0, 0, Time.deltaTime * -speed));
				yield return new WaitForSeconds(0.0f);
			}
		}
		while (transform.rotation.z <= 0)
		{
			transform.Rotate(new Vector3(0, 0, Time.deltaTime * speed));
			yield return new WaitForSeconds(0.0f);
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (activated && !onGround && !isChild)
		{
			if (other.gameObject.tag.Equals("Enemy"))
			{
				Instantiate(deathParticle, transform.position, Quaternion.identity);
				other.gameObject.GetComponent<EnemyController>().DeactivateMe();
				DeactivateMe();
			}
			else if (other.gameObject.tag.Equals("Player"))
			{
				Instantiate(deathParticle, transform.position, Quaternion.identity);
				other.gameObject.GetComponent<PlayerController>().PlayerDied();
			}
			else if (other.gameObject.tag.Equals("Foreground"))
			{
				Instantiate(deathParticle, transform.position, Quaternion.identity);
				DeactivateMe(); // ANYTHING UNDER THESE WON'T BE EXECUTED
				onGround = true;
				gameObject.transform.localScale = gameObject.transform.localScale / 2;
				CreateChilds();
				gameObject.transform.position = gameObject.transform.position + new Vector3(0, -2, 0);
			}
		}
		else if (isChild)
		{
			if (other.gameObject.tag.Equals("Foreground"))
			{
				onGround = true;
			}
		}
	}

	public void SetMeToBeChild()
	{
		isChild = true;
	}

	private void CreateChilds()
	{
		//for (int i = -1; i <= 1; i += 2)
		//{
		//	GameObject child = Instantiate(prefab);
		//	child.transform.localScale = child.transform.localScale / 2;

		//	float x = i * Random.Range(2, 3);

		//	child.GetComponent<AvalanceController>().SetMeToBeChild();
		//	child.transform.parent = gameObject.transform;
		//	child.transform.position = new Vector3(gameObject.transform.position.x + x, gameObject.transform.position.y, gameObject.transform.position.z);
		//	child.transform.Rotate(new Vector3(0, 0, Random.value * 2));
		//}
		return;
	}

	public void DeactivateMe()
	{
		Fabric.EventManager.Instance.PostEvent("AvalanceDrop", Fabric.EventAction.StopSound, null, gameObject);
		Fabric.EventManager.Instance.PostEvent("AvalanceBreaks", Fabric.EventAction.PlaySound, null, gameObject);

		GameManager.ChangeCurrentStateToFalse(ID);
		gameObject.SetActive(false);
	}

	public void Save()
	{
		savedStateOfActivated = activated;
		savedPosition = transform.position;
		savedScale = transform.localScale;
		savedStateOfActivateOnCollision = activateOnCollision;
		savedStateOfOnGround = onGround;

		savedStateOfTimerActivated = timerActivated;
		savedTimeBeforeObjectCanBeActivated = timeBeforeObjectCanBeActivated;
	}

	public void Restart()
	{
		if (transform.position != savedPosition)
		{
			transform.position = savedPosition;
			foreach (Transform child in gameObject.transform)
			{
				Destroy(child.gameObject);
			}
		}

		transform.localScale = savedScale;
		activated = savedStateOfActivated;
		activateOnCollision = savedStateOfActivateOnCollision;
		onGround = savedStateOfOnGround;

		timerActivated = savedStateOfTimerActivated;
		timeBeforeObjectCanBeActivated = savedTimeBeforeObjectCanBeActivated;
	}

	public void SetID(int value)
	{
		ID = value;
	}

	public int GetID()
	{
		return ID;
	}
}
