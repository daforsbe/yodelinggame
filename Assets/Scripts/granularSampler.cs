﻿using UnityEngine;
using System.Collections;
public class granularSampler : MonoBehaviour
{
	AudioSource thisAudioSource;
	AudioClip soundGrain;
	public int samplerate = 44100;
	public float frequency = 440;
	public int pointsToAnalyze = 100;
	public int meanDifferenceAnalysisDifference = 1024;
	public float dafaultsGLength = 0.2f;
	public bool debugMode = true;
	public float defaultStartPoint = 0f;
	float currentLoopScore = 0;
	int currentSelectedLoopLength = 0;
	int goneThrough = 0;
	int selectedZero = 0;


	int currentLoopStart;
	int currentLoopEnd;
	int currentLoopLength;

	int playHeadPosition;

	private bool initialized = false;
	public float sampleStartControlValue = 0.0f;
	//private float loopEndSliderValue = 0.3f;   // JUST TO GET RID OF WARNINGS
	private float loopLengthSliderValue = 0f;

	//private float playHeaderSliderValue=0.0f;

	public float startingPoint = 0.92f;
	public bool mute = false;

    float[] originalSamples;
    //this flag indcates wether the playhead should copy the nextChunk into the nowChunk the next time it finishes playing the nowchunk
    private bool nextChunkFlag = true;
    //is the chunk of audio that is currently being looped on filterRead
    private float[] nowChunk= { 0f,0f };
    //is the chunk that contains the loop in the current position of the granular sampler start.
    private float[] nextChunk = { 0f,0f };

    void Start()
	{
		thisAudioSource = GetComponent<AudioSource>();
		originalSamples = new float[thisAudioSource.clip.samples * thisAudioSource.clip.channels];
		playHeadPosition = 0;
		//currentLoopStart = 0;
		loopLengthSliderValue = dafaultsGLength;
		//currentLoopEnd = 4342;
		thisAudioSource.clip.GetData(originalSamples, 0);
		initialized = true;
        sampleStartControlValue = (defaultStartPoint);
        setStartingPoint(sampleStartControlValue);
        initialized = true;
    }

	//THIS IS ONLY FOR DEBUGGING FROM EDITOR - REMOVE WHEN EVERYTHING CONFIRMED
	//the following lines need to be eliminated after tests because is not efficient
	//void Update()
	//{
	//	setStartingPoint(sampleStartControlValue);
	//}

	
	void Update()
	{
		if(startingPoint < 1 && !mute)
		{
			if (startingPoint < 0)
				startingPoint = 0;

			startingPoint += Time.deltaTime * 0.5f;
			setStartingPoint(startingPoint);
		}
	}

	public void DecreaseStartingPoint()
	{
		startingPoint -= 0.5f;
	}

    int controlLength = 100;

    void OnGUI()
	{
		if (debugMode)
		{

			int margin = 25;
			int inMarginWidth = Screen.width - 2 * margin;
			int cline = 0;
			setStartingPoint(GUI.HorizontalSlider(new Rect(margin, cline, inMarginWidth, margin), sampleStartControlValue, 0.0f, 1.0f));
			cline += margin;
			//loopEndSliderValue = GUI.HorizontalSlider(new Rect(25, 50, 500, 30),Mathf.Max(loopStartSliderValue,loopEndSliderValue), 0.0f, 1.0f);
			/*playHeaderSliderValue =*/
			GUI.HorizontalSlider(new Rect(margin, cline, inMarginWidth, margin), ((playHeadPosition * 2.00f) / originalSamples.Length), 0.0f, 1.0f);
			cline += margin;
            GUI.SetNextControlName("glength");
            controlLength = int.Parse(GUI.TextField(new Rect(margin, cline, inMarginWidth, margin), ""+ controlLength, 25));
            if (GUI.GetNameOfFocusedControl() == "glength")
            {
                setGrainLength(controlLength);
            }
            //loopLengthSliderValue = GUI.HorizontalSlider(new Rect(margin, cline, inMarginWidth, margin), loopLengthSliderValue, 0.0f, 1.0f);
			cline += margin;
			GUI.HorizontalSlider(new Rect(margin, cline, inMarginWidth, margin), panLfo, -1.0f, 1.0f);
			cline += margin;

			//setGrainPosition(Mathf.FloorToInt(loopStartSliderValue * originalSamples.Length / 2));


			//currentLoopStart = Mathf.FloorToInt (loopStartSliderValue * originalSamples.Length / 2);
			//currentLoopEnd = Mathf.FloorToInt (loopEndSliderValue * originalSamples.Length / 2);


			GUI.TextArea(new Rect(margin, cline, inMarginWidth, 200),
				"star:" + currentLoopStart
				+ "\nend:" + currentLoopEnd 
                + "\nlen:" + (currentLoopEnd - currentLoopStart) + ")"
				+ "\npos:" + playHeadPosition
				//+ "\nLen:" + (originalSamples.Length / 2)
				+ "\n Length would be:" + currentSelectedLoopLength
				+ "\n Similarity score is:" + currentLoopScore + " an " + goneThrough + "sel " + selectedZero
				+ "\n panlfo" + panLfo
				);
		}
	}
	public void setStartingPoint(float num)
	{

		sampleStartControlValue = Mathf.Clamp(num, 0, 1);
		setGrainPosition(Mathf.FloorToInt(sampleStartControlValue * originalSamples.Length / 2));
		setGrainLength(Mathf.FloorToInt(loopLengthSliderValue * originalSamples.Length / 2));
	}
	int getNextPositiveZeroCrossing(int sample)
	{
		//search for next zero crossing from sample
		int zeroCrossingFound = -1;
		int searchPoint = sample;

		while (zeroCrossingFound == -1)
		{
			//we need to waste one index to make zero crossing search easier, thus count before
			searchPoint++;
			if (searchPoint >= originalSamples.Length)
			{
				return -1;
			}
			//if either this sample is zero or the two samples have different sign, it's a zero cross.
			//good idea sourced from https://www.dsprelated.com/showcode/179.php
			if (originalSamples[searchPoint - 1] * originalSamples[searchPoint] < 0)
			{
				zeroCrossingFound = searchPoint;
				return zeroCrossingFound;
			}
		}
		return searchPoint;
	}
	public void setGrainPosition(int sample)
	{
		currentLoopStart = getNextPositiveZeroCrossing(sample);
	}
	void setGrainLength(int slen)
	{
		currentSelectedLoopLength = slen;
		goneThrough = 0;

		int[] zeroCrossingList = new int[pointsToAnalyze];
		int ZeroCrossingSeekPosition = slen + currentLoopStart;
		//list some zero crossings (the amount to list is user defined)
		for (int a = 0; a < zeroCrossingList.Length; a++)
		{
			zeroCrossingList[a] = getNextPositiveZeroCrossing(ZeroCrossingSeekPosition);
			//if there was a zero crossing found, next repetition we search zero crossing from this point on.
			if (zeroCrossingList[a] != -1)
			{
				ZeroCrossingSeekPosition = zeroCrossingList[a];
			};
		}
		//here we store the most similar starting point found so far.
		int mostSimilarStartingPointInList = 0;
		mostSimilarStartingPointInList += 0; // JUST TO GET RID OF WARNINGS
		//the lower score, the more similar
		float mostSimilarStartingPointScore = 99999999;
		//now analyze each zero crossing with our starting point, to find the most similar according to mean difference

		//for each zero cross in the list. zeroCrossingList[b] will be an individual sample position. 
		for (int b = 0; b < zeroCrossingList.Length; b++)
		{
			float thisDifference = 0;
			//analze now sample by sample
			for (int c = 0; c < meanDifferenceAnalysisDifference; c++)
			{
				int comparisonHeadA = currentLoopStart + c;
				int comparisonHeadB = zeroCrossingList[b] + c;
				//just make sure we are not out of range
				if (!((comparisonHeadA >= originalSamples.Length) || (comparisonHeadB >= originalSamples.Length)))
				{
					goneThrough++;
					//the first points affect the most, while the last points don't affect so much. I don't know what decay curve to use 
					float wheight = ((c * 0.1f) / meanDifferenceAnalysisDifference);
					wheight = wheight * wheight;
					thisDifference += Mathf.Abs(originalSamples[comparisonHeadA] - originalSamples[comparisonHeadB]) * wheight;
				}
				else
				{
					c = meanDifferenceAnalysisDifference + 1;
				}
				//if we already are scoring worse than the best, skip this evaluation
				if (thisDifference > mostSimilarStartingPointScore)
				{
					c = meanDifferenceAnalysisDifference + 1;
				}
			}
			if (thisDifference < mostSimilarStartingPointScore)
			{
				mostSimilarStartingPointScore = thisDifference;
				mostSimilarStartingPointInList = zeroCrossingList[b];
				selectedZero = b;
			}
		}
		currentLoopScore = mostSimilarStartingPointScore;
		currentLoopEnd = zeroCrossingList[selectedZero];
		currentLoopLength = currentLoopEnd - currentLoopStart;
        //prevent trying to copy more than the available samples.
        int clampedLength = Mathf.Min(currentLoopLength * 2, originalSamples.Length- currentLoopEnd*2);
        
        if (clampedLength > 1){
            nextChunk = new float[clampedLength];
            System.Array.ConstrainedCopy(originalSamples, currentLoopStart * 2, nextChunk, 0, clampedLength);
            //chamfer edges: soften the start and endpoints of the loop to avoid the sound of the gap.
            int edgeChamfer = 100;//in samples
            int chLen = nextChunk.Length;
            edgeChamfer = Mathf.Min(edgeChamfer, chLen / 2);
            for(int a = 0; a < edgeChamfer*2; a+=2){
                //here it would be nice to fade out and in the original sample, or to apply a lowpassFilter
                float portion = 0.5f * a / edgeChamfer;
                //start edge 
                nextChunk[a] *= portion;
                nextChunk[a+1] *= portion;
                //and end edge
                nextChunk[(chLen - a) - 2] *= portion;
                nextChunk[(chLen - a) - 1] *= portion;
                
            }



            nextChunkFlag = true;
        }
        

        //currentLoopLength += 0;   // JUST TO GET RID OF WARNINGS
    }
    //int nonLoopedPlayHeader = 0;
    //the realtime filter uses it to avoid stuter from the remaining gap.
    private float panLfo = 0;
    private int relativePosition = 0;
    private int currentChunkLength = 0;
    //when the chunk is changed, the antipositioned sampler could jump drastically unless it remembers the last chunk after change.
    //private float[] antiChunk = new float[0];
    //private bool antiChunkFlag = false;
    void OnAudioFilterRead(float[] data, int channels)
	{
        if (initialized)
        {
            for (int a = 0; a < data.Length; a += 2)
            {
                panLfo = 1;//0.5f + Mathf.Cos((Mathf.PI * 2 / currentChunkLength) * relativePosition) / -2f;
                //position to read in the nowChunk
                relativePosition++;
                //relativePosition = relativePosition % currentLoopLength;


                //chunk looper
                if (relativePosition >= currentChunkLength)
                {
                    relativePosition = 0;
                    if (nextChunkFlag)
                    {
                        //System.Array.Copy(nextChunk,nowChunk,currentChunkLength);

                        nowChunk = nextChunk;
                        currentChunkLength = nowChunk.Length / 2;
                        nextChunkFlag = false;
                       // antiChunkFlag = true;
                    }
                }
                //absolute position in the originalsample
                playHeadPosition = currentLoopStart + relativePosition;
                //sample position is the current sample in the current audio channel to read.
                int sampleposition = relativePosition * 2;

                //get the other half to blend with
                /* if (relativePosition <= (currentLoopLength / 2))
                 {
                     otherHalf = sampleposition - (currentLoopLength / 2);
                 }
                 else {
                     otherHalf = sampleposition + (currentLoopLength / 2);
                 }
                 */
                /*int antiposition = (sampleposition + (currentChunkLength / 2)) % currentChunkLength;*/
                //change antichunk once finished playing
                /*if (antiChunkFlag)
                    if (antiposition >= antiChunk.Length)
                    {
                        antiChunk = nowChunk;
                        antiChunkFlag = false;
                    }*/
                //float antiPanLfo = (1 - panLfo);
                data[a] = (nowChunk[sampleposition] * panLfo);// + (nowChunk[antiposition] * antiPanLfo)) / 2*/;//+ (originalSamples[otherHalf] * (1-panLfo));
                data[a + 1] = (nowChunk[sampleposition + 1] * panLfo);// + (nowChunk[antiposition + 1] * antiPanLfo)) / 2*/;//+ (originalSamples[otherHalf+1] * (1-panLfo));
                playHeadPosition++;

            }
        }
    }
}
