﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SmoothFollow : MonoBehaviour
{
	public Transform target;
	public float smooth = 1.0f;
	public Vector3 offset = Vector3.zero;
	
	private float z = 0.0f;
	private Vector2 position;

	void Awake()
	{
		z = transform.position.z;
		position = transform.position;
	}

	void Start()
	{
		if (target != null)
		{
			SetPos(target.position);
		}
	}
	
	void LateUpdate()
	{
		if (target != null)
		{
			SetPos(Vector2.Lerp(position, target.position, Time.deltaTime / smooth));
		}
	}

	public void SetPos(Vector2 pos)
	{
		position = pos;
		transform.position = pos.ToVector3(z) + offset;
	}

	public IEnumerator MoveToPosition(Vector2 targetPosition, float duration)
	{
		Vector2 startPos = position;
		for (float f = 0.0f; f < duration; f += Time.deltaTime)
		{
			float t = Mathf.SmoothStep( 0.0f, 1.0f, f / duration );
			Vector2 pos = Vector2.Lerp(startPos, targetPosition, t);
			SetPos(pos);
			yield return null;
		}
		SetPos(targetPosition);
	}

	public IEnumerator MoveToPositionCurve(Vector2 targetPosition, float duration, AnimationCurve curve)
	{
		Vector2 startPos = position;
		for (float f = 0.0f; f < duration; f += Time.deltaTime)
		{
			float t = curve.Evaluate(f / duration);
			Vector2 pos = Vector2.Lerp(startPos, targetPosition, t);
			SetPos(pos);
			yield return null;
		}
		SetPos(targetPosition);
	}
}
