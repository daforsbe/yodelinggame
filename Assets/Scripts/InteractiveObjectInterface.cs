﻿using UnityEngine;
using System.Collections;

public interface InteractiveObject {

	void Save();
	void Restart();

	void SetID(int value);
	int GetID();
}
