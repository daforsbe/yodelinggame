﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DelayedDestroy : MonoBehaviour
{
	public float delay = 1.0f;

	void Start()
	{
		StartCoroutine(DestroyRoutine());
	}

	private IEnumerator DestroyRoutine()
	{
		yield return new WaitForSeconds(delay);
		Destroy(gameObject);
	}
	
}
