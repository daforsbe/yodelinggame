﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent(typeof(Camera))]
public class VisibilityCamera : MonoBehaviour
{
	public RawImage targetImage;

	private Camera cam;

	private RenderTexture rt = null;

	void Awake()
	{
		cam = GetComponent<Camera>();
	}

	void Update()
	{
		if (rt == null || rt.width != Screen.width || rt.height != Screen.height)
		{
			if (rt != null)
			{
				rt.Release();
			}

			rt = new RenderTexture(Screen.width, Screen.height, 24);
			cam.targetTexture = rt;
			targetImage.texture = rt;
		}
	}
}
