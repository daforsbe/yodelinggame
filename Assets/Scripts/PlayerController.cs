﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour, InteractiveObject
{
	[Header("Yodle audio")]
	public bool granular1 = true;
	public bool yodle = false;

	[Header("Inputs")]
	public bool WASDMovement = true;
    public bool JoystickInput;

	[Header("Aiming")]
	public Transform playerSprite;
	public SpriteRenderer yodelingSprite;
	public float rotationSmooth = 0.1f;

	[Header("Movement")]
	public float gravity = 50.0f;
	public float sideMovementForce = 75.0f;
	public float upMovementForce = 200.0f;
	public float downMovementForce = 20.0f;
	public float maxSpeed = 50.0f;

	[Header("Shouting")]
	public WaveSource waveSource;
	public bool allowedToShoot = true;

	[Header("Echo force")]
	public float echoForceStrength = 3.0f;
	public float playerCircleRadius = 2.0f;
	public AnimationCurve echoForceFalloff;
	public AnimationCurve SUPERFALLOFF;
	public AnimationCurve particleTimeFalloff;
	public LayerMask particleLayers;

	[Header("Terrain forces")]
	public float terrainForce = 1000.0f;
	public AnimationCurve terrainForceCurve;
	public int terrainRayCount = 36;
	public float terrainRayDistance = 8.0f;
	public LayerMask terrainLayers;

	[Header("Animation")]
	public float idleForceX = 10.0f;
	public float idleForceUp = 10.0f;
	public float idleForceDown = 20.0f;
	private Animator animator;

	public GameObject deathVisiblityPrefab;

	private Rigidbody2D rb;
	private Vector2 inputDirection = Vector2.zero;
	private bool validInputDirection = false;
	private Vector2 netForce = Vector2.zero;
	private bool dead = false;
	private bool SUPERFEATHER = false;

	public delegate void SpeedChangedHandle(float speed);
	public static event SpeedChangedHandle OnSpeedChanged;

	[Header("Particle Systems")]
	public GameObject deathParticle;
	public GameObject featherParticle;

	[Header("Audio")]
	public GameObject playerAudio;

	// Checkpoint variables
	private Vector3 savedPosition;
	private float savedEchoForce;
	private bool savedSUPER;
	private GameManagerScript GameManager;
	private int ID;

	void Start()
	{
		Save();
		GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerScript>();
		GameManager.AddObjectToList(this.gameObject);

		rb = GetComponent<Rigidbody2D>();
		animator = playerSprite.GetComponent<Animator>();

		playerSprite.localScale = Vector3.one;
	}

	void Update()
	{
		UpdateInputs();
		UpdateRotation();
		UpdateShouting();
    }

	void FixedUpdate()
	{
		netForce = Vector2.zero;
		UpdateForces();
		CheckSpeedLimit();
		UpdateAnimator();
	}
	
	void UpdateAnimator()
	{
		float angle = GetAngle(Vector2.up, netForce);
		
		int dir = -1;
		if (Mathf.Abs( netForce.x ) <= idleForceX && 
			(netForce.y > 0 ? netForce.y <= idleForceUp : -netForce.y <= idleForceDown))
		{
			dir = 0;
		}
		else if (Mathf.Abs(angle) < 45)
		{
			// Up
			dir = 2;
		}
		else if (Mathf.Abs(angle) < 135)
		{
			// Side
			dir = 1;
		}
		else
		{
			// Down
			dir = 4;
		}
		animator.SetInteger("direction", dir);

		animator.SetBool("feather", allowedToShoot);
	}

	private static float GetAngle(Vector2 v1, Vector2 v2)
	{
		var sign = Mathf.Sign(v1.x * v2.y - v1.y * v2.x);
		return Vector2.Angle(v1, v2) * sign;
	}

	private void UpdateInputs()
	{
        if (JoystickInput && Joystick.ShootingJoystick != null)
        {
            inputDirection = Joystick.ShootingJoystick.OutputDirection;
            validInputDirection = inputDirection.sqrMagnitude > 0.001f;
        }
        else
        {
            if (Input.mousePresent)
            {
                Vector2 mouseViewportPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                Vector2 playerViewportPos = Camera.main.WorldToViewportPoint(waveSource.transform.position);
                inputDirection = (mouseViewportPos - playerViewportPos).normalized;
                validInputDirection = inputDirection.sqrMagnitude > 0.001f;
            }
            else
            {
                inputDirection = Vector2.zero;
                validInputDirection = false;
            }
        }
    }

    private void UpdateRotation()
	{
		if (validInputDirection)
		{
			Quaternion targetRotation = Quaternion.LookRotation(Vector3.forward, -inputDirection);
			waveSource.transform.rotation = Quaternion.Slerp(waveSource.transform.rotation, targetRotation, Time.deltaTime / rotationSmooth);
		}

		Vector3 localScale = playerSprite.localScale;
		if (localScale.x > 0 && rb.velocity.x < -0.1f)
		{
			localScale.x *= -1;
		}
		else if (localScale.x < 0 && rb.velocity.x > 0.1f)
		{
			localScale.x *= -1;
		}
		playerSprite.localScale = localScale;
	}

	private void UpdateForces()
	{
		float verticalTerrainForce = 0.0f;

		// Gravity
		rb.AddForce(gravity * Vector2.down);
		netForce += gravity * Vector2.down;

		// Terrain interaction
		if (!Physics2D.OverlapPoint(transform.position, terrainLayers))
		{
			Vector2 force = EvaluateEnvironmentForces();
			rb.AddForce(force * terrainForce);
			netForce += force * terrainForce;
			verticalTerrainForce = force.y;
		}
		else
		{
			rb.AddForce(Vector2.up * terrainForce);
		}

		// Normal movement
		EvaluateMovementForce(verticalTerrainForce);

		// Echo force
		Collider2D[] particles = Physics2D.OverlapCircleAll(transform.position, playerCircleRadius, particleLayers);
		if (particles.Length > 0)
		{
			Vector3 averageCenter = Vector3.zero;
			Vector2 echoDirection = Vector2.zero;
			foreach (var particle in particles)
			{
				float relativeTime = particle.GetComponent<Particle>().relativeTime;
				float fallOff = 1.0f;
				if (SUPERFEATHER)
					fallOff = SUPERFALLOFF.Evaluate(relativeTime);
				else
					fallOff = particleTimeFalloff.Evaluate(relativeTime);
				Vector2 velocityDir = particle.GetComponent<Rigidbody2D>().velocity.normalized;
				echoDirection += fallOff * velocityDir;
				averageCenter += particle.transform.position;
			}
			echoDirection /= particles.Length;
			averageCenter /= particles.Length;
			float distance = Vector2.Distance(averageCenter, transform.position);
			float falloff = echoForceFalloff.Evaluate(distance / playerCircleRadius);
			rb.AddForce(echoDirection * echoForceStrength * falloff);
			netForce += echoDirection * echoForceStrength * falloff;
		}
		
		if (OnSpeedChanged != null)
			OnSpeedChanged(rb.velocity.magnitude);
	}
	
	private Vector2 EvaluateEnvironmentForces()
	{
		Vector2 acceleration = Vector2.zero;
		float rayDeltaAngle = 360.0f / terrainRayCount;
		for (int rayIndex = 0; rayIndex < terrainRayCount; ++rayIndex)
		{
			float angle = rayDeltaAngle * rayIndex;
			Vector2 direction = Quaternion.AngleAxis(angle, Vector3.forward) * Vector2.up;
			RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, terrainRayDistance, terrainLayers);

			if (hit)
			{
				float normalizedDistance = hit.distance / terrainRayDistance;
				float attenuation = terrainForceCurve.Evaluate(normalizedDistance);
				acceleration += hit.normal * attenuation / terrainRayCount;
				Debug.DrawLine(transform.position + direction.ToVector3() * 1.5f, hit.point, Color.gray);
				Debug.DrawRay(hit.point, hit.normal * attenuation * 20.0f, Color.white);
			}
		}
		return acceleration;
	}

	private void EvaluateMovementForce(float verticalTerrainForce)
	{
		Vector2 movementInput = Vector2.zero;

		if (WASDMovement)
		{
			float horizontal = Input.GetAxis("Horizontal");
			float vertical = Input.GetAxis("Vertical");
			movementInput = new Vector2(horizontal, vertical);
		}
        else if (JoystickInput)
        {
            movementInput = Joystick.MovementJoystick.OutputDirection;
        }
		else
		{
			if (GetMovementInput() && validInputDirection)
				movementInput = inputDirection;
		}

		if (movementInput.sqrMagnitude > 0.001f)
		{
			float verticalMovementLimit = Mathf.Clamp(25.0f * verticalTerrainForce, 0.0f, 1.0f);
			movementInput.y = Mathf.Min(movementInput.y, verticalMovementLimit);
			movementInput.x *= sideMovementForce;
			movementInput.y *= movementInput.y > 0 ? upMovementForce : downMovementForce;
			rb.AddForce( movementInput );
			netForce += movementInput;
		}
	}

	private void UpdateShouting()
	{
		if (GetShootInput() && validInputDirection)
		{
			if(yodle)
				Fabric.EventManager.Instance.PostEvent("Yodle", Fabric.EventAction.PlaySound, null, this.gameObject);
			else {
				if(granular1 == true)
					gameObject.transform.Find("YodelVoice").GetComponent<granularSampler>().DecreaseStartingPoint();
				else
					gameObject.transform.Find("YodelVoice").GetComponent<granularSampler2>().DecreaseStartingPoint();
			}


			waveSource.EmitWave(rb.velocity, SUPERFEATHER);
		}
	}
	
	private bool GetMovementInput()
	{
		return Input.GetMouseButton(0);
	}

	private bool GetShootInput()
	{
        if (JoystickInput)
        {
            if (Joystick.ShootingJoystick == null)
                return false;

            bool shoot = Joystick.ShootingJoystick.ReleasedInCurrentFrame;
            Joystick.ShootingJoystick.ReleasedInCurrentFrame = false;
            if(shoot)
                Joystick.ShootingJoystick.OutputDirection = Vector3.zero;

            return shoot && allowedToShoot;
        }
		return Input.GetMouseButtonDown(WASDMovement ? 0 : 1) && allowedToShoot;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (dead)
			return;

		if (other.tag.Equals("Enemy") || other.tag.Equals("Moving Enemy"))
		{
			var visiblityGO = Instantiate(deathVisiblityPrefab);
			visiblityGO.transform.position = other.transform.position;
			PlayerDied();
		}
		if (other.tag.Equals("Feather"))
		{
			if (allowedToShoot == false)
			{
				StartCoroutine(EnableYodelingSprite());
			}
			else
			{
				echoForceStrength += other.GetComponent<FeatherController>().echoForceIncrease;
			}
			allowedToShoot = true;
            GameManager.ShootingJoystick.gameObject.SetActive(true);
			Instantiate(featherParticle, other.gameObject.transform.position, Quaternion.identity);
			other.GetComponent<FeatherController>().DeactivateMe();
		}
		if (other.GetComponent<SuperFeather>())
		{
			SUPERFEATHER = true;
		}
		if (other.tag.Equals("Goal"))
		{
			GameManager.WinGame();
		}
		if (other.tag.Equals("FireAudioTrigger"))
		{
			if (other.gameObject.transform.parent.gameObject.GetComponent<CheckpointController>().IsActivated())
			{
				Fabric.EventManager.Instance.PostEvent("FireActivated", Fabric.EventAction.PlaySound, null, gameObject);
			}
			else
			{
				Fabric.EventManager.Instance.PostEvent("FireNotActivated", Fabric.EventAction.PlaySound, null, gameObject);
			}
		}
		if (other.tag.Equals("GoatAudioTrigger"))
		{
			if (other.gameObject.transform.parent.Find("proto_sprsht_1_4").transform.Find("VisibilitySprite").gameObject.activeSelf)
			{
				Fabric.EventManager.Instance.PostEvent("GoatActivated", Fabric.EventAction.PlaySound, null, gameObject);
			}
			else
			{
				Fabric.EventManager.Instance.PostEvent("GoatNotActivated", Fabric.EventAction.PlaySound, null, gameObject);
			}
		}
		if (other.tag.Equals("StaticEnemyAudioTrigger"))
		{
			Fabric.EventManager.Instance.PostEvent("StaticEnemy", Fabric.EventAction.PlaySound, null, gameObject);
		}
		if (other.tag.Equals("MovingEnemyAudioTrigger"))
		{
			if (other.transform.parent.parent.GetComponent<EnemyController>().IsActivated())
			{
				Fabric.EventManager.Instance.PostEvent("MovingEnemyMoving", Fabric.EventAction.PlaySound, null, gameObject);
			}
			else
			{
				Fabric.EventManager.Instance.PostEvent("MovingEnemyStill", Fabric.EventAction.PlaySound, null, gameObject);
			}
		}
		if (other.tag.Equals("HouseAudioTrigger"))
		{
			Fabric.EventManager.Instance.PostEvent("NearHouse", Fabric.EventAction.PlaySound, null, gameObject);
		}

	}

	private IEnumerator EnableYodelingSprite()
	{
		StartCoroutine(GameManager.FadeIn(GameManager.mouse));

		float targetAlpha = yodelingSprite.color.a;
		SetYodelingSpriteAlpha(0.0f);
		yodelingSprite.enabled = true;
		for (float f = 0; f < 0.5f; f += Time.deltaTime)
		{
			float alpha = Mathf.SmoothStep(0.0f, targetAlpha, f / 0.5f);
			SetYodelingSpriteAlpha(alpha);
			yield return null;
		}
		SetYodelingSpriteAlpha(targetAlpha);
	}

	private void SetYodelingSpriteAlpha(float a)
	{
		Color c = yodelingSprite.color;
		c.a = a;
		yodelingSprite.color = c;
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag.Equals("FireAudioTrigger"))
		{
			if (other.gameObject.transform.parent.gameObject.GetComponent<CheckpointController>().IsActivated())
			{
				Fabric.EventManager.Instance.PostEvent("FireActivated", Fabric.EventAction.StopSound, null, gameObject);
			}
			else
			{
				Fabric.EventManager.Instance.PostEvent("FireNotActivated", Fabric.EventAction.StopSound, null, gameObject);
			}
		}
		if (other.tag.Equals("GoatAudioTrigger"))
		{
			if (other.gameObject.transform.parent.Find("proto_sprsht_1_4").transform.Find("VisibilitySprite").gameObject.activeSelf)
			{
				Fabric.EventManager.Instance.PostEvent("GoatActivated", Fabric.EventAction.StopSound, null, gameObject);
			}
			else
			{
				Fabric.EventManager.Instance.PostEvent("GoatNotActivated", Fabric.EventAction.StopSound, null, gameObject);
			}
		}
		if (other.tag.Equals("StaticEnemyAudioTrigger"))
		{
			Fabric.EventManager.Instance.PostEvent("StaticEnemy", Fabric.EventAction.StopSound, null, gameObject);
		}
		if (other.tag.Equals("MovingEnemyAudioTrigger"))
		{
			if (other.transform.parent.parent.GetComponent<EnemyController>().IsActivated())
			{
				Fabric.EventManager.Instance.PostEvent("MovingEnemyMoving", Fabric.EventAction.StopSound, null, gameObject);
			}
			else
			{
				Fabric.EventManager.Instance.PostEvent("MovingEnemyStill", Fabric.EventAction.StopSound, null, gameObject);
			}
		}
		if (other.tag.Equals("HouseAudioTrigger"))
		{
			Fabric.EventManager.Instance.PostEvent("NearHouse", Fabric.EventAction.StopSound, null, gameObject);
		}
	}

	private void CheckSpeedLimit()
	{
		if (rb.velocity.magnitude > maxSpeed)
		{
			rb.velocity = rb.velocity.normalized * maxSpeed;
		}
	}

	// Implement InteractiveObject
	public void Save()
	{
		savedPosition = transform.position;
		savedEchoForce = echoForceStrength;
		savedSUPER = SUPERFEATHER;
	}

	public void Restart()
	{
		StartCoroutine(MoveToPosition(savedPosition, 1.0f));
		rb.velocity = Vector2.zero;
		echoForceStrength = savedEchoForce;
		SUPERFEATHER = savedSUPER;
	}

	public IEnumerator MoveToPosition(Vector3 targetPosition, float duration)
	{
		Vector3 startPos = transform.position;
		for (float f = 0.0f; f < duration; f += Time.deltaTime)
		{
			float t = Mathf.SmoothStep(0.0f, 1.0f, f / duration);
			Vector3 pos = Vector3.Lerp(startPos, targetPosition, t);
			transform.position = pos;
			yield return null;
		}
		transform.position = targetPosition;
		dead = false;
	}

	public void PlayerDied()
	{
		dead = true;
		Fabric.EventManager.Instance.PostEvent("PlayerDead", Fabric.EventAction.PlaySound, null, gameObject);
		Instantiate(deathParticle, transform.position, Quaternion.identity);
		GameManager.PlayDeathAnimationAndRestart();
	}
		
	public void SetID(int value)
	{
		ID = value;
	}

	public int GetID()
	{
		return ID;
	}
}
