﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(LineRenderer))]
public class Wave : MonoBehaviour
{
	public float relativeBreakDistance = 3.0f;
	public float lifeTime = 5.0f;
	public float fadeDuration = 1.0f;

	private LineRenderer lineRenderer;
	private Particle[] particles = new Particle[0];
	private float time = 0.0f;
	private bool destroyed = false;

	void Awake()
	{
		lineRenderer = GetComponent<LineRenderer>();
	}

	public void Initialize()
	{
		particles = GetComponentsInChildren<Particle>(true);
		particles = particles.OrderBy(o => o.index).ToArray();
	}

	public void Update()
	{
		time += Time.deltaTime;
		if (time >= lifeTime || particles.Length <= 1)
		{
			if (!destroyed)
			{
				destroyed = true;
				Destroy(gameObject);
			}
			return;
		}

		if (time > lifeTime - fadeDuration)
		{
			float t = (time - (lifeTime - fadeDuration)) / fadeDuration;
			Color c = Color.white;
			c.a = 1.0f - t;
			c *= c.a;
			lineRenderer.SetColors(c, c);
		}

		float averageDistance = 0.0f;
		for (int i = 1; i < particles.Length; ++i)
		{
			Vector3 prevPos = particles[i-1].transform.position;
			Vector3 pos = particles[i].transform.position;
			float distance = Vector3.Distance(prevPos, pos);
			averageDistance += distance;
		}
		averageDistance /= (float)particles.Length;

		int breakIndex = -1;
		for (int i = 1; i < particles.Length; ++i)
		{
			Vector3 prevPos = particles[i - 1].transform.position;
			Vector3 pos = particles[i].transform.position;
			float distance = Vector3.Distance(prevPos, pos);
			if (distance > relativeBreakDistance * averageDistance)
			{
				breakIndex = i;
				break;
			}
		}

		if (breakIndex > 0)
		{
			GameObject duplicateWave = Instantiate(gameObject);
			Particle[] duplicateParticles = duplicateWave.GetComponentsInChildren<Particle>(true);
			duplicateParticles = duplicateParticles.OrderBy(o => o.index).ToArray();

			for (int i = 0; i < duplicateParticles.Length; ++i)
			{
				var original = particles[i].GetComponent<Rigidbody2D>();
				var dupe = duplicateParticles[i].GetComponent<Rigidbody2D>();
				dupe.velocity = original.velocity;
			}
			
			foreach (var dupeParticle in duplicateParticles)
			{
				if (dupeParticle.GetComponent<Particle>().index < breakIndex)
				{
					DestroyImmediate(dupeParticle.gameObject);
				}
				else
				{
					dupeParticle.GetComponent<Particle>().index -= breakIndex;
				}
			}

			duplicateWave.GetComponent<Wave>().time = time;
			duplicateWave.GetComponent<Wave>().Initialize();
			duplicateWave.GetComponent<Wave>().Update();

			foreach (var particle in particles)
			{
				if (particle.GetComponent<Particle>().index >= breakIndex)
				{
					DestroyImmediate(particle.gameObject);
				}
			}

			Initialize();
		}

		lineRenderer.SetVertexCount(particles.Length);
		for (int i = 0; i < particles.Length; ++i)
		{
			Vector3 pos = particles[i].transform.position;
			lineRenderer.SetPosition(i, pos);
			particles[i].relativeTime = Mathf.Clamp(time / lifeTime, 0, 1);
		}
	}

}
