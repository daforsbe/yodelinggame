﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Particle : MonoBehaviour
{
	private static readonly float cooldownTime = 1.0f;

	public GameObject collisionPrefab;
	
	[HideInInspector]
	public int index = 0;

	private float cooldownTimer = -1.0f;

	public float relativeTime = 0.0f;

	void Update()
	{
		if (cooldownTimer > 0.0f)
			cooldownTimer -= Time.deltaTime;
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (cooldownTimer >= 0.0f)
			return;

		cooldownTimer = cooldownTime;

		GameObject collisionGO = Instantiate(collisionPrefab);
		collisionGO.transform.position = collision.contacts[0].point;
		
		if (collision.gameObject.tag.Equals("Avalance")) {
			collision.gameObject.GetComponent<AvalanceController>().ActivateMe();
		}
	}


	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag.Equals("Goat"))
		{
			col.GetComponent<GoatController>().ActivateVisibility();
		}
		else if (col.tag.Equals("Moving Enemy"))
		{
			col.GetComponent<EnemyController>().ActivateMe();
		}
	}
}
