﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaveSource : MonoBehaviour
{
	public GameObject wavePrefab;
	public GameObject particlePrefab;
	public float waveSpeed = 30.0f;
	public float initialOffset = 0.1f;
	public float tessellation = 1.0f;
	public float waveCooldown = 0.2f;

	private float cooldown = 0.0f;

	void Update()
	{
		if (cooldown > 0.0f)
		{
			cooldown -= Time.deltaTime;
		}
	}

	public bool EmitWave(Vector2 playerVelocity, bool SUPERFEATHER)
	{
		if (cooldown > 0.0f)
			return false;
		cooldown = waveCooldown;

		float aimAngle = 25.0f;

		Quaternion minRotation = Quaternion.AngleAxis(-aimAngle, Vector3.forward);
		Quaternion maxRotation = Quaternion.AngleAxis(aimAngle, Vector3.forward);

		GameObject waveGO = Instantiate(wavePrefab);

		int particleCount = (int)(tessellation * aimAngle);
		for (int i = 0; i < particleCount; ++i)
		{
			float t = (float)i / particleCount;
			
			Quaternion rotation = Quaternion.Slerp(minRotation, maxRotation, t);
			Vector3 direction = -(transform.rotation * rotation * Vector3.up).normalized;
			Vector2 velocity = direction * waveSpeed * (SUPERFEATHER ? 1.5f : 1.0f);
			Vector3 position = transform.position + direction * initialOffset;
			
			GameObject particle = Instantiate(particlePrefab);
			particle.transform.SetParent(waveGO.transform);
			particle.transform.position = position;
			particle.GetComponent<Rigidbody2D>().velocity = velocity + playerVelocity;
			particle.GetComponent<Particle>().index = i;
		}

		if (SUPERFEATHER)
			waveGO.GetComponent<Wave>().lifeTime = 60.0f;

		waveGO.GetComponent<Wave>().Initialize();
		waveGO.GetComponent<Wave>().Update();

		return true;
	}

}
