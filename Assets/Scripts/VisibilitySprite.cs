﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VisibilitySprite : MonoBehaviour
{
	public float smooth = 1.0f;
	public float minSpeed = 0.1f;
	public float maxSpeed = 10.0f;
	public float minSize = 0.1f;
	public float maxSize = 1.5f;

	public SpriteRenderer[] sprites = new SpriteRenderer[0];

	void Start()
	{
		PlayerController.OnSpeedChanged += OnSpeedChanged;
	}

	void OnDestroy()
	{
		PlayerController.OnSpeedChanged -= OnSpeedChanged;
	}

	private void OnSpeedChanged(float speed)
	{
		float size = Utils.RemapClamped(minSpeed, maxSpeed, maxSize, minSize, speed);
		transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(size, size, size), Time.deltaTime / smooth);
	}

	public void SetAlpha(float a)
	{
		foreach (var s in sprites)
		{
			s.material.SetFloat("Fade", 1.0f - a );
		}
	}
}
