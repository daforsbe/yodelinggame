﻿using UnityEngine;
using System.Collections;

public class StartScreen : MonoBehaviour
{
	[System.Serializable]
	public class IntroElement
	{
		public IntroElement()
		{
			canvasGroup = null;
			startTime = 3.0f;
			endTime = 5.0f;
			fadeDuration = 0.5f;
		}

		public CanvasGroup canvasGroup;
		public float startTime;
		public float endTime;
		public float fadeDuration;
	}

	public CanvasGroup startTextGroup;
	public float startTextFadeDuration = 0.5f;
	public IntroElement[] introElements = new IntroElement[0];

	void Start()
	{
		foreach (var e in introElements)
			e.canvasGroup.alpha = 0.0f;
	}

	public void ShowStart()
	{
		Show(startTextGroup, startTextFadeDuration);
	}

	public void HideStart()
	{
		Hide(startTextGroup, startTextFadeDuration);
	}

	public void PlayIntroSequence()
	{
		foreach (var e in introElements)
			StartCoroutine(PlayIntroElementRoutine(e));
	}

	private IEnumerator PlayIntroElementRoutine(IntroElement e)
	{
		yield return new WaitForSeconds(e.startTime);
		Show(e.canvasGroup, e.fadeDuration);
		float remainingTime = e.endTime - e.startTime - e.fadeDuration;
		yield return new WaitForSeconds(remainingTime);
		Hide(e.canvasGroup, e.fadeDuration);
	}

	private void Show(CanvasGroup group, float fadeDuration)
	{
		StartCoroutine(Fade(group, 1.0f, fadeDuration));
	}

	private void Hide(CanvasGroup group, float fadeDuration)
	{
		StartCoroutine(Fade(group, 0.0f, fadeDuration));
	}

	private IEnumerator Fade(CanvasGroup group, float targetAlpha, float fadeDuration)
	{
		float startAlpha = group.alpha;
		for (float f = 0.0f; f < fadeDuration; f += Time.deltaTime)
		{
			float t = f / fadeDuration;
			float alpha = Mathf.Lerp(startAlpha, targetAlpha, t);
			group.alpha = alpha;
			yield return null;
		}
		group.alpha = targetAlpha;
	}
}
