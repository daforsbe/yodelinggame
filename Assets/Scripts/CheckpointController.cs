﻿using UnityEngine;
using System.Collections;

public class CheckpointController : MonoBehaviour
{

	private bool activated = false;


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag.Equals("Player"))
		{
			if (!activated)
			{
				Fabric.EventManager.Instance.PostEvent("FireActivationSound", Fabric.EventAction.PlaySound, null, other.gameObject);

				Fabric.EventManager.Instance.PostEvent("FireNotActivated", Fabric.EventAction.StopSound, null, other.gameObject);
				Fabric.EventManager.Instance.PostEvent("FireActivated", Fabric.EventAction.PlaySound, null, other.gameObject);

				GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerScript>().SaveAll();
				activated = true;
				transform.Find("Fire_Particle").gameObject.SetActive(true);
			}

		}
	}

	public bool IsActivated()
	{
		return activated;
	}
}
