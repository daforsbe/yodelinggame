﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour
{
	public bool randomStart = true;
	public float speed = 1.0f;
	public AnimationCurve curve = AnimationCurve.Linear(0, 0, 1, 1);

	private Quaternion startLocalRot;
	private float offset;

	void Start()
	{
		startLocalRot = transform.localRotation;
		offset = Random.Range(0, 1);
	}
	
	void Update()
	{
		float angle = 360.0f * curve.Evaluate(Time.time * speed + offset);
		Quaternion rot = Quaternion.AngleAxis(angle, Vector3.forward);
		transform.localRotation = startLocalRot * rot;
	}
}
