﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Renderer))]
public class SortingLayer : MonoBehaviour
{
	public string sortingLayerName = string.Empty;

	void Awake()
	{
		if (!string.IsNullOrEmpty(sortingLayerName))
		{
			GetComponent<Renderer>().sortingLayerName = sortingLayerName;
		}
	}
}
