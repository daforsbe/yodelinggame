﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{
	public bool startWithIntroScreen = true;
	public SmoothFollow mainCamera;
	public PlayerController playerController;
	public Material playerMaterial;
	public VisibilitySprite playerVisibility;
	public SpriteRenderer wasd;
	public SpriteRenderer mouse;
	public AnimationCurve cameraCurve;
	public RawImage visibilityTexture;
    public Joystick MovementJoystick;
    public Joystick ShootingJoystick;

	[Header("Intro")]
	public float cameraTransitionDuration = 3.0f;

	[Header("Player Spawn")]
	public AnimationCurve playerSpawnCurve;
	public float playerSpawnDuration = 1.0f;
	public float playerSpawnYOffset = -3.0f;
	public float playerVisibilityFadeDuration = 1.0f;
	
	// Second parameter (bool) indicates (saved/current state) information whether object is active or not 
	private List<Tuple<InteractiveObject, bool>> savedObjectList = new List<Tuple<InteractiveObject, bool>>();
	private List<Tuple<GameObject, bool>> currentStateObjectList = new List<Tuple<GameObject, bool>>(new Tuple<GameObject, bool>[1000]);

	private bool startScreenActive = false;
	private GameObject startScreenCamPos;
	private StartScreen startScreen;

	void Start()
	{
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;
		transform.localScale = Vector3.one;

		startScreenCamPos = GameObject.FindGameObjectWithTag("StartScreenCameraPos");
		startScreen = FindObjectOfType<StartScreen>();

		if (startWithIntroScreen)
		{
			IntroInit();
		}
		else
		{
			SetPlayerAlpha(1.0f);
		}

		playerController.allowedToShoot = !startWithIntroScreen;
        ShootingJoystick.gameObject.SetActive(!startWithIntroScreen);
        playerController.yodelingSprite.enabled = false;
	}

	void Update()
	{
		if (startScreenActive)
		{
			if (Input.GetMouseButton(0))
			{
				startScreen.HideStart();
				startScreenActive = false;
				startScreen.PlayIntroSequence();
				StartCoroutine(IntroRoutine());
			}
		}
		if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.R))
		{
			GameObject audio = GameObject.Find("Audio");
			Destroy(audio);
			SceneManager.LoadScene("Main");
		}
	}

	private void IntroInit()
	{
		mainCamera.target = null;
		mainCamera.SetPos(startScreenCamPos.transform.position);
		playerController.enabled = false;
        MovementJoystick.gameObject.SetActive(false);
        startScreenActive = true;
		SetPlayerAlpha(0.0f);
		playerVisibility.SetAlpha(0.0f);
		SetAlpha(wasd, 0.0f);
		SetAlpha(mouse, 0.0f);
	}

	public void WinGame()
	{
		StartCoroutine(WinGameRoutine());
	}

	private IEnumerator WinGameRoutine()
	{
		playerController.enabled = false;
        MovementJoystick.gameObject.SetActive(false);
        yield return PlayerFadeRoutine(1.0f, 0.0f);
		yield return new WaitForSeconds(0.5f);
		yield return Fade(visibilityTexture, 1, 0);

		mainCamera.target = null;
		StartCoroutine( mainCamera.MoveToPositionCurve(startScreenCamPos.transform.position, 40.0f, cameraCurve ));
		yield return new WaitForSeconds(30.0f);
		StartCoroutine(Fade(visibilityTexture, 0, 1));
		yield return new WaitForSeconds(10.0f);
		
		GameObject audio = GameObject.Find("Audio");
		Destroy(audio);
		SceneManager.LoadScene("Main");
	}

	private IEnumerator IntroRoutine()
	{
		Debug.Log("Starting intro");
		
		mainCamera.target = null;

		Vector2 targetPos = playerController.transform.position;
		yield return mainCamera.MoveToPositionCurve(targetPos, cameraTransitionDuration, cameraCurve);

		yield return PlayerSpawnRoutine();

		mainCamera.target = playerController.transform;
        playerController.enabled = true;
        MovementJoystick.gameObject.SetActive(true);

		yield return FadeIn(wasd);

		Debug.Log("Ending intro");
	}

	private IEnumerator PlayerSpawnRoutine()
	{
		Vector2 startPos = playerController.transform.position + Vector3.up * playerSpawnYOffset;
		Vector2 endPos = playerController.transform.position;
		for (float f = 0.0f; f < playerSpawnDuration; f += Time.deltaTime)
		{
			float t = playerSpawnCurve.Evaluate( f / playerSpawnDuration );

			Vector2 pos = Vector3.Lerp(startPos, endPos, t );
			playerController.GetComponent<Rigidbody2D>().MovePosition(pos);

			SetPlayerAlpha(t);
			playerVisibility.SetAlpha(t);

			yield return null;
		}
		playerController.GetComponent<Rigidbody2D>().MovePosition(endPos);
		
		playerVisibility.SetAlpha(1.0f);
		SetPlayerAlpha(1.0f);
	}

	private void SetPlayerAlpha(float f)
	{
		Color c = playerMaterial.color;
		c.a = f;
		playerMaterial.color = c;
	}

	//ONLY FOR DEBUGGING
	//void Update()
	//{
	//	if (Input.GetKeyDown(KeyCode.Space))
	//	{
	//		foreach(Tuple<InteractiveObject, bool> el in savedObjectList)
	//		{
	//			print("saved: " + el.second);
	//			print("current: " + currentStateObjectList[el.first.GetID()].second);
	//		}
	//		print("-----------");
	//	}
	//}

	public void AddObjectToList(GameObject obj)
	{
		InteractiveObject objectInterface = (InteractiveObject)obj.GetComponent(typeof(InteractiveObject));
		int index = savedObjectList.Count;
		objectInterface.SetID(index);

		savedObjectList.Add(new Tuple<InteractiveObject, bool>(objectInterface, true));
		currentStateObjectList[index] = new Tuple<GameObject, bool>(obj, true);
	}

	public void SaveAll()
	{
		foreach (Tuple<InteractiveObject, bool> tuple in savedObjectList)
		{
			InteractiveObject obj = tuple.first;
			obj.Save();
			tuple.second = currentStateObjectList[obj.GetID()].second;
		}
	}

	public void PlayDeathAnimationAndRestart()
	{
		StartCoroutine(DeathRoutine());
	}

	private IEnumerator DeathRoutine()
	{
		playerController.enabled = false;
        MovementJoystick.gameObject.SetActive(false);
        yield return PlayerFadeRoutine(1.0f, 0.0f);
		yield return new WaitForSeconds(0.2f);

		RestartAll();

		yield return new WaitForSeconds(1.5f);

		playerController.enabled = true;
        MovementJoystick.gameObject.SetActive(true);
        yield return PlayerFadeRoutine(0.0f, 1.0f);
	}
	
	private IEnumerator PlayerFadeRoutine(float startAlpha, float endAlpha)
	{
		const float duration = 0.5f;
		for (float f = 0.0f; f < duration; f += Time.deltaTime)
		{
			float t = Mathf.SmoothStep(0.0f, 1.0f, f / duration);
			float alpha = Mathf.Lerp(startAlpha, endAlpha, t);
			SetPlayerAlpha(alpha);
			playerVisibility.SetAlpha(alpha);
			yield return null;
		}
		SetPlayerAlpha(endAlpha);
		playerVisibility.SetAlpha(endAlpha);
	}

	public IEnumerator FadeIn(SpriteRenderer r)
	{
		const float duration = 0.5f;
		for (float f = 0.0f; f < duration; f += Time.deltaTime)
		{
			float t = Mathf.SmoothStep(0.0f, 1.0f, f / duration);
			float alpha = Mathf.Lerp(0.0f, 1.0f, t);
			SetAlpha(r, alpha);
			yield return null;
		}
		SetAlpha(r, 1.0f);
	}

	private void SetAlpha(SpriteRenderer r, float a)
	{
		Color c = r.color;
		c.a = a;
		r.color = c;
	}

	public IEnumerator Fade(RawImage r, float start, float end)
	{
		const float duration = 2.0f;
		for (float f = 0.0f; f < duration; f += Time.deltaTime)
		{
			float t = Mathf.SmoothStep(0.0f, 1.0f, f / duration);
			float alpha = Mathf.Lerp(start, end, t);
			SetAlpha(r, alpha);
			yield return null;
		}
		SetAlpha(r, end);
	}
	
	private void SetAlpha(RawImage r, float a)
	{
		Color c = r.color;
		c.a = a;
		r.color = c;
	}

	public void RestartAll()
	{
		foreach (GameObject wave in GameObject.FindGameObjectsWithTag("Wave"))
		{
			Destroy(wave);
		}

		foreach (Tuple<InteractiveObject, bool> tuple in savedObjectList)
		{
			InteractiveObject obj = tuple.first;
			currentStateObjectList[obj.GetID()].second = tuple.second;
			currentStateObjectList[obj.GetID()].first.SetActive(tuple.second);
			obj.Restart();
		}
	}

	public void ChangeCurrentStateToFalse(int index)
	{
		currentStateObjectList[index].second = false;
	}
}
