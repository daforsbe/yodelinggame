﻿using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour
{
	public float speed;
	Vector3[] path;
	int targetIndex;

	private bool activated = false;
	public float maxDistanceFromGoatToGetCaptured = 10;
	public float fractionOfMaxDistanceFromGoatToStopMoving = 2;

	private bool facingRight = true;

	private Transform target;
	private GameObject goats;

	void Start()
	{
		// Okay, so this is not the greatest way to do this but 
		// WAAAY better than manually assigning them every time....
		goats = GameObject.Find("Goats");
		if (goats == null)
			Debug.LogError("Gameobject Goats not found!");
		target = FindObjectOfType<PlayerController>().transform;
	}

	void Update()
	{
		if (activated)
		{
			for (int i = 0; i < goats.transform.childCount; i++)
			{
				GameObject goat = goats.transform.GetChild(i).gameObject;
				if (goat.transform.Find("proto_sprsht_1_4").Find("VisibilitySprite").gameObject.activeSelf)
				{
					if (distanceBetween(goat, this.gameObject) <= maxDistanceFromGoatToGetCaptured)
					{
						if (distanceBetween(goat, this.gameObject) <= maxDistanceFromGoatToGetCaptured / fractionOfMaxDistanceFromGoatToStopMoving)
						{
							activated = false;
							gameObject.GetComponent<EnemyController>().SetBeingCaptured();
						}
						transform.Find("MovingEnemy_1").gameObject.GetComponent<Animator>().SetBool("activated", false);
						target = goat.transform;
						break;
					}
				}
			}

			PathRequestManager.RequestPath(transform.position, target.position, OnPathFound);
		}
		else
		{
			path = new Vector3[0];
		}
	}

	private float distanceBetween(GameObject enemy, GameObject goat)
	{
		return Mathf.Sqrt(Mathf.Pow(enemy.transform.position.x - goat.transform.position.x, 2) + Mathf.Pow(enemy.transform.position.y - goat.transform.position.y, 2));
	}

	public void OnPathFound(Vector3[] newPath, bool pathSuccessful)
	{
		if (pathSuccessful)
		{
			path = newPath;
			targetIndex = 0;
			StopCoroutine("FollowPath");
			StartCoroutine("FollowPath");
		}
	}

	IEnumerator FollowPath()
	{
		if (!activated)
		{
			yield break;
		}
		Vector3 currentWaypoint;
		try
		{
			currentWaypoint = path[0];
		}
		catch
		{
			yield break;
		}
		while (true)
		{
			if (transform.position == currentWaypoint)
			{
				targetIndex++;
				if (targetIndex >= path.Length)
				{
					yield break;
				}
				currentWaypoint = path[targetIndex];
			}

			transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);
			if(currentWaypoint.x < transform.position.x && facingRight || currentWaypoint.x > transform.position.x && !facingRight)
			{
				FlipGameobject();
			}
			yield return null;

		}
	}

	private void FlipGameobject()
	{
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		facingRight = !facingRight;
	}

	public void OnDrawGizmos()
	{
		if (path != null)
		{
			for (int i = targetIndex; i < path.Length; i++)
			{
				Gizmos.color = Color.black;
				Gizmos.DrawCube(path[i], Vector3.one);

				if (i == targetIndex)
				{
					Gizmos.DrawLine(transform.position, path[i]);
				}
				else
				{
					Gizmos.DrawLine(path[i - 1], path[i]);
				}
			}
		}
	}

	public void ActivateMe(bool state)
	{
		activated = state;
	}
}