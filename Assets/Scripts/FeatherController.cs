﻿using UnityEngine;
using System.Collections;

public class FeatherController : MonoBehaviour, InteractiveObject
{
	public float echoForceIncrease = 10.0f;
	 
	// Checkpoint variables
	private Vector3 savedPosition;
	private bool savedStateOfActivated;
	private GameManagerScript GameManager;
	private int ID;
	

	void Start () {
		GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerScript>();
		GameManager.AddObjectToList(this.gameObject);
		Save();
	}


	public void DeactivateMe()
	{
		Fabric.EventManager.Instance.PostEvent("FeatherPickUp", Fabric.EventAction.PlaySound, null, gameObject);

		GameManager.ChangeCurrentStateToFalse(ID);
		gameObject.SetActive(false);
	}
	

	// Implement InteractiveObject
	public void Save()
	{
		savedPosition = transform.position;
	}

	public void Restart()
	{
		transform.position = savedPosition;
	}

	public void SetID(int value)
	{
		ID = value;
	}

	public int GetID()
	{
		return ID;
	}
}
