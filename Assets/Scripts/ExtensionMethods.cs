﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ExtensionMethods
{
	public static Vector3 ToVector3(this Vector2 v, float z = 0.0f)
	{
		return new Vector3(v.x, v.y, z);
	}
}
