﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyController : MonoBehaviour, InteractiveObject
{
	public bool activated = false;
	public GameObject deathParticle;
	public bool captured = false;

	// Checkpoint variables
	private GameManagerScript GameManager;
	private bool savedStateOfActivated;
	private bool savedStateOfCaptured;
	private Vector3 savedPosition;
	private int ID;

	public enum Type { Moving, Static};
	public Type type;

	private Animator animator;
	private SpriteRenderer spriteRenderer;

	private GameObject movingEnemies;

	private bool activateMeCalled = false;

	void Start()
	{
		movingEnemies = GameObject.Find("Moving Enemies");
		if (movingEnemies == null)
			Debug.LogError("Gameobject 'Moving Enemies' not found!");

		GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerScript>();
		GameManager.AddObjectToList(this.gameObject);
		animator = GetComponentInChildren<Animator>();
		spriteRenderer = GetComponentInChildren<SpriteRenderer>();

		// Save enemy positions only at the start
		// to prevent player from getting stuck if
		// a moving enemy is right next to them when
		// activating a checkpoint
		savedStateOfActivated = activated;
		savedStateOfCaptured = captured;
		savedPosition = transform.position;

		StartAnimatorWithRandomPoint();
	}

	private void StartAnimatorWithRandomPoint()
	{
		if (type == Type.Static)
		{
			AnimatorClipInfo[] cInfoBody = GetComponent<Animator>().GetCurrentAnimatorClipInfo(0);
			AnimatorClipInfo[] cInfoHands = transform.Find("static_hands").gameObject.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0);
			AnimatorClipInfo[] cInfoVisibility = transform.Find("VisibilitySprite").gameObject.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0);

			AnimationClip clipBody = cInfoBody[0].clip;
			AnimationClip clipHands = cInfoHands[0].clip;
			AnimationClip clipVisibility = cInfoVisibility[0].clip;

			float startingPos = clipBody.length * Random.value;

			GetComponent<Animator>().Play(clipBody.name, 0, startingPos);
			transform.Find("static_hands").gameObject.GetComponent<Animator>().Play(clipHands.name, 0, startingPos);
			transform.Find("VisibilitySprite").gameObject.GetComponent<Animator>().Play(clipVisibility.name, 0, startingPos);
		}
	}

	//----------------------------------------------------------------
	// Update function ONLY for testing activating from editor
	//void Update()
	//{
	//	if (type == Type.Moving)
	//	{
	//		if (activated)
	//		{
	//			GetComponent<Unit>().ActivateMe(true);
	//		}
	//		else
	//		{
	//			GetComponent<Unit>().ActivateMe(false);
	//		}
	//	}
	//}
	//----------------------------------------------------------------

	public bool IsActivated()
	{
		return activated;
	}

	void Update()
	{
		if (activateMeCalled)
		{
			activateMeCalled = false;

			if (type == Type.Moving && !captured)
			{
				DeactivateAllEnemyMovements();

				activated = true;
				animator.SetBool("activated", true);
				GetComponent<Unit>().ActivateMe(true);
			}
		}
	}

	public void ActivateMe()
	{
		activateMeCalled = true;
	}

	public void SetBeingCaptured()
	{
		activated = false;
		captured = true;
	}

	private void DeactivateAllEnemyMovements()
	{
		for (int i = 0; i < movingEnemies.transform.childCount; i++)
		{
			GameObject enemy = movingEnemies.transform.GetChild(i).gameObject;
			enemy.transform.Find("MovingEnemy").gameObject.GetComponent<EnemyController>().DeactivateMe();
		}
	}

	public void DeactivateMe()
	{
		if(type == Type.Static)
		{
			Fabric.EventManager.Instance.PostEvent("EnemyDead", Fabric.EventAction.PlaySound, null, gameObject);
			GameManager.ChangeCurrentStateToFalse(ID);
			Instantiate(deathParticle, transform.position, Quaternion.identity);
			gameObject.SetActive(false);
		}
		else
		{
			activated = false;
			GetComponent<Unit>().ActivateMe(false);
		}
	}

	public void Save()
	{
		
	}

	public void Restart()
	{
		activated = savedStateOfActivated;
		captured = savedStateOfCaptured;

		if (type == Type.Moving)
			StartCoroutine(RestartRoutine());
		else
			transform.position = savedPosition;

		if (type == Type.Moving)
		{
			GetComponent<Unit>().ActivateMe(activated);
		}
	}

	private IEnumerator RestartRoutine()
	{
		yield return new WaitForSeconds(0.5f);
		yield return FadeRoutine(1.0f, 0.0f);
		transform.position = savedPosition;
		yield return FadeRoutine(0.0f, 1.0f);
	}

	private IEnumerator FadeRoutine(float startAlpha, float endAlpha)
	{
		const float duration = 0.5f;
		for (float f = 0.0f; f < duration; f += Time.deltaTime)
		{
			float t = Mathf.SmoothStep(0.0f, 1.0f, f / duration);
			float alpha = Mathf.Lerp(startAlpha, endAlpha, t);
			SetAlpha(alpha);
			yield return null;
		}
		SetAlpha(endAlpha);
	}

	private void SetAlpha(float a)
	{
		Color c = spriteRenderer.material.color;
		c.a = a;
		spriteRenderer.material.color = c;
	}

	public void SetID(int value)
	{
		ID = value;
	}

	public int GetID()
	{
		return ID;
	}
}
