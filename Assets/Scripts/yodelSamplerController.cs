﻿using UnityEngine;
using System.Collections;



public class yodelSamplerController : MonoBehaviour {
    public float posLfoFrequency = 20f;
    public float posLfoAmplitude = 0.001f;
    public float posLfoBias = 0.5f;
    public granularSampler sampler;
    private float lfo = 0;
    public GUIStyle controllerStylea;
    public GUIStyle controllerStyleb;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        lfo = Mathf.Sin(posLfoFrequency*Time.time*2*Mathf.PI)*posLfoAmplitude+posLfoBias;
        sampler.setStartingPoint(lfo);
	}
    void OnGUI() {
        int margin = 25;
        int inMarginWidth = Screen.width - 2 * margin;
        int cline = 120;
        posLfoBias = GUI.HorizontalSlider(new Rect(margin, cline, inMarginWidth, 30), posLfoBias, 0.0f, 1.0f, controllerStylea, controllerStyleb);
    }
}
