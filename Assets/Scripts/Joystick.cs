﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {

    public static Joystick MovementJoystick;
    public static Joystick ShootingJoystick;

    public bool IsMovementJoystick;
    public bool IsShootingJoystick;

    public GameObject outsideJoystick;
    public GameObject innerJoystick;

    [HideInInspector]
    public Vector2 OutputDirection;
    [HideInInspector]
    public bool ReleasedInCurrentFrame = false;

    private Vector2 startPosition;
    private Vector2 defaultPosition;
    private float maxDistance;

    private void Awake()
    {
        if (IsMovementJoystick)
        {
            if (MovementJoystick == null)
            {
                MovementJoystick = this;
            }
            else if (!MovementJoystick.Equals(this))
            {
                Destroy(gameObject);
                return;
            }
        }
        else if (IsShootingJoystick)
        {
            if (ShootingJoystick == null)
            {
                ShootingJoystick = this;
            }
            else if (!ShootingJoystick.Equals(this))
            {
                Destroy(gameObject);
                return;
            }
        }
    }

    private void Start()
    {
        defaultPosition = outsideJoystick.transform.position;
        maxDistance = outsideJoystick.GetComponent<Image>().rectTransform.sizeDelta.x
            - innerJoystick.GetComponent<Image>().rectTransform.sizeDelta.x;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        startPosition = eventData.position;
        outsideJoystick.transform.position = startPosition;
        innerJoystick.transform.position = startPosition;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        outsideJoystick.transform.position = defaultPosition;
        innerJoystick.transform.position = defaultPosition;
        ReleasedInCurrentFrame = true;
        if (IsMovementJoystick)
        {
            OutputDirection = Vector2.zero;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 dir = eventData.position - startPosition;
        if (Mathf.Abs(dir.x) > maxDistance)
        {
            dir = new Vector2(maxDistance * dir.x / Mathf.Abs(dir.x), dir.y);
        }
        if (Mathf.Abs(dir.y) > maxDistance)
        {
            dir = new Vector2(dir.x, maxDistance * dir.y / Mathf.Abs(dir.y));
        }
        innerJoystick.transform.position = startPosition + dir;

        OutputDirection = dir / maxDistance;
    }
}
