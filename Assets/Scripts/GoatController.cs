﻿using UnityEngine;
using System.Collections;

public class GoatController : MonoBehaviour, InteractiveObject
{
	public float visibilityGrowthDuration = 3;
	public AnimationCurve revealVisibility;

	private Transform VisibilitySprite;
	private Vector3 finalSizeOfVisibility;
	private bool activateVisibilityCalled = false;

	// Checkpoint variables
	private GameManagerScript GameManager;
	private bool savedStateOfActivated;
	private int ID;


	void Start()
	{
		VisibilitySprite = transform.Find("proto_sprsht_1_4").transform.Find("VisibilitySprite");

		GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerScript>();
		GameManager.AddObjectToList(this.gameObject);
		Save();

		finalSizeOfVisibility = VisibilitySprite.transform.localScale;
		StartAnimatorWithRandomPoint();
	}

	void Update()
	{
		if (activateVisibilityCalled)
		{
			activateVisibilityCalled = false;

			if (!VisibilitySprite.gameObject.activeSelf)
			{
				Fabric.EventManager.Instance.PostEvent("GoatActivationSound", Fabric.EventAction.PlaySound, null, gameObject);

				Fabric.EventManager.Instance.PostEvent("GoatNotActivated", Fabric.EventAction.StopSound, null, gameObject);
				Fabric.EventManager.Instance.PostEvent("GoatActivated", Fabric.EventAction.PlaySound, null, gameObject);

				AnimationActivated(true);

				VisibilitySprite.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
				VisibilitySprite.gameObject.SetActive(true);
				StartCoroutine(ResizeVisibilitySprite(finalSizeOfVisibility, revealVisibility));
			}
		}
	}

	private void StartAnimatorWithRandomPoint()
	{
		AnimatorClipInfo[] cInfo = transform.Find("proto_sprsht_1_4").GetComponent<Animator>().GetCurrentAnimatorClipInfo(0);
		AnimationClip clip = cInfo[0].clip;
		transform.Find("proto_sprsht_1_4").GetComponent<Animator>().Play(clip.name, 0, clip.length * Random.value);
	}

	public void ActivateVisibility()
	{
		activateVisibilityCalled = true;
	}

	private void AnimationActivated(bool active)
	{
		gameObject.transform.Find("proto_sprsht_1_4").gameObject.GetComponent<Animator>().SetBool("activated", active);
	}

	IEnumerator ResizeVisibilitySprite(Vector3 targetScale, AnimationCurve curve)
	{
		Vector3 startScale = VisibilitySprite.transform.localScale;
		float duration = visibilityGrowthDuration;

		for( float f = 0.0f; f < duration; f += Time.deltaTime)
		{
			float t = curve.Evaluate( f / duration);
			Vector3 scale = Vector3.Lerp(startScale, targetScale, t);
			VisibilitySprite.transform.localScale = scale;
			yield return null;
		}

		VisibilitySprite.transform.localScale = targetScale;
	}

	IEnumerator FadeOutVisibilitySprite(SpriteRenderer renderer)
	{
		float duration = 1.0f;
		for (float f = 0.0f; f < duration; f += Time.deltaTime)
		{
			float fade = Mathf.SmoothStep(0.0f, 1.0f, f / duration);
			SetVisibilityFade(renderer, fade);
			yield return null;
		}
		SetVisibilityFade(renderer, 1.0f);
	}

	private void SetVisibilityFade(SpriteRenderer rend, float alpha)
	{
		rend.material.SetFloat("Fade", alpha);
	}
	
	public void Save()
	{
		savedStateOfActivated = VisibilitySprite.gameObject.activeSelf;
	}

	public void Restart()
	{
		StartCoroutine(RestartRoutine());
		AnimationActivated(savedStateOfActivated);
	}

	private IEnumerator RestartRoutine()
	{
		if (savedStateOfActivated == false)
		{
			SpriteRenderer[] renderers = VisibilitySprite.GetComponentsInChildren<SpriteRenderer>(true);
			foreach (var r in renderers)
				StartCoroutine(FadeOutVisibilitySprite(r));
			yield return new WaitForSeconds(1.5f);

			VisibilitySprite.gameObject.SetActive(savedStateOfActivated);

			foreach (var r in renderers)
				SetVisibilityFade(r, 0.0f);
		}
		else
		{
			VisibilitySprite.gameObject.SetActive(savedStateOfActivated);
		}
	}

	public void SetID(int value)
	{
		ID = value;
	}

	public int GetID()
	{
		return ID;
	}

}
